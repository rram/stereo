// Sriramajayam

#include <str_CameraModule.h>
#include <iostream>

// Commandline arguments
const cv::String keys =
  "{boardxml | | XML file for charucoboard }"
  "{help h usage man ? | | Calibration using a charucoboard}";

int main(int argc, char** argv)
{
  // Read directory to use for files from commandline
  cv::CommandLineParser cl(argc, argv, keys);
  // Help
  if(cl.has("help")) { cl.printMessage(); return 0; }
  
  // Get the board XML filename
  CV_Assert(cl.has("boardxml"));
  cv::String boardxml = cl.get<cv::String>("boardxml");
  std::cout<<"\nCharucoboard read from: "<<boardxml<<"\n"<<std::flush;

  // Create a board
  str::Charucoboard cb(boardxml);

  // Right camera
  std::cout<<"\nCalibrating right camera..."<<std::flush;
  str::Camera rcam;
  str::CharucoboardCorners rcam_corners;
  rcam.Calibrate(cb, "rcam/calib_charuco/charuco_calib.xml", rcam_corners);
  rcam.Save("rcam/calib_charuco/camera.xml");
  
  // Left camera
  std::cout<<"\nCalibrating left camera..."<<std::flush;
  str::Camera lcam;
  str::CharucoboardCorners lcam_corners;
  lcam.Calibrate(cb, "lcam/calib_charuco/charuco_calib.xml", lcam_corners);
  lcam.Save("lcam/calib_charuco/camera.xml");
  
  // Refine to common corners
  std::cout<<"\nStereo calibration..."<<std::flush;
  str::CharucoboardCorners lcam_comm_corners, rcam_comm_corners;
  str::CharucoboardCorners::Match(lcam_corners, rcam_corners,
				 lcam_comm_corners, rcam_comm_corners);
  
  str::StereoRig rig(lcam, lcam_comm_corners, rcam, rcam_comm_corners);
  rig.Save("stereo_rig_charuco.xml");
}


