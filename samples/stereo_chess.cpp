// Sriramajayam

#include <str_CameraModule.h>

int main()
{
  // Left camera & corners
  str::Camera lcam("lcam/calib_chess/camera.xml");
  str::ChessboardCorners lcam_corners("lcam/calib/corners.xml");
  
  // Right camera & corners
  str::Camera rcam("rcam/calib_chess/camera.xml");
  str::ChessboardCorners rcam_corners("rcam/calib/corners.xml");
  
  // Stereo calibration
  str::StereoRig rig(lcam, lcam_corners,
		    rcam, rcam_corners);
  rig.Save("stereo_rig_chess.xml");
}
