// Sriramajayam

#include <str_CameraModule.h>
#include <iostream>

int main()
{
  // Create a board
  str::Charucoboard cb("charucoboard.xml");

  // Right camera
  std::cout<<"\nCalibrating right camera..."<<std::flush;
  str::Camera rcam;
  str::CharucoboardCorners rcam_corners;
  rcam.Calibrate(cb, "charuco/rcam/charuco_calib.xml", rcam_corners);
  rcam.Save("charuco/rcam/camera.xml");
  
  // Left camera
  std::cout<<"\nCalibrating left camera..."<<std::flush;
  str::Camera lcam;
  str::CharucoboardCorners lcam_corners;
  lcam.Calibrate(cb, "charuco/lcam/charuco_calib.xml", lcam_corners);
  lcam.Save("charuco/lcam/camera.xml");
  
  // Refine to common corners
  std::cout<<"\nStereo calibration..."<<std::flush;
  str::CharucoboardCorners lcam_comm_corners, rcam_comm_corners;
  str::CharucoboardCorners::Match(lcam_corners, rcam_corners,
				 lcam_comm_corners, rcam_comm_corners);
  
  str::StereoRig rig(lcam, lcam_comm_corners, rcam, rcam_comm_corners);
  rig.Save("stereo_rig_charuco.xml");
}


