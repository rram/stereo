// Sriramajayam

#include <str_CameraModule.h>

int main()
{
  // Left camera & corners
  str::Camera lcam("chess/lcam/camera.xml");
  str::ChessboardCorners lcam_corners("chess/lcam/camera_corners.xml");
  
  // Right camera & corners
  str::Camera rcam("chess/rcam/camera.xml");
  str::ChessboardCorners rcam_corners("chess/rcam/camera_corners.xml");
  
  // Stereo calibration
  str::StereoRig rig(lcam, lcam_corners,
		    rcam, rcam_corners);
  rig.Save("stereorig_chess.xml");
}
