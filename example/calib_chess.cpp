// Sriramajayam

#include <str_CameraModule.h>

int main()
{
  // Create chessboard
  cv::String filename = "chess/rcam/chessboard.xml";
  str::Chessboard cb(filename);
  
  // Create camera and corners for chessboard
  str::Camera cam;
  str::ChessboardCorners corners;
  CV_Assert(!cam.IsCalibrated());
  cam.Calibrate(cb, corners);
  CV_Assert(cam.IsCalibrated());
  cam.Save("camera.xml");
  corners.Save("camera_corners.xml");
}
