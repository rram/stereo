// Sriramajayam

#include <str_StereoRig.h>
#include <str_CharucoboardCorners.h>
#include <opencv2/calib3d.hpp>
#include <iostream>

namespace str
{
  // Constructor
  StereoRig::StereoRig(const Camera& lcam,
		       const CharucoboardCorners& lcorners,
		       const Camera& rcam,
		       const CharucoboardCorners& rcorners)
    :_lcam(lcam), _rcam(rcam)
  {
    // Both cameras should be calibrated
    CV_Assert((_lcam.IsCalibrated() && _rcam.IsCalibrated()) &&
	      "str::StereoRig::StereoRig()- Cams not calibrated.");

    // Copy image size
    CV_Assert(_lcam.GetImageSize()==_rcam.GetImageSize() &&
	      "str::StereRig::StereoRig()- Inconsistent image size.");
    _imgSize = _lcam.GetImageSize();
    
    // chessboard points imaged.
    // 3D points should be the same in both
    const auto& pts3D = lcorners.corners3D;
    const auto& lpts2D = lcorners.corners2D;
    const auto& rpts2D = rcorners.corners2D;
    
    // Stereo calibration
    const auto& lK = _lcam.GetCameraMatrix();
    const auto& rK = _rcam.GetCameraMatrix();
    const auto& ldistCoeffs = _lcam.GetDistortionCoeffs();
    const auto& rdistCoeffs = _rcam.GetDistortionCoeffs();
    const auto term = cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 100, 1.e-5);

    // Calibrate without altering camera instrinsics
    double rmsErr =
      cv::stereoCalibrate(pts3D, lpts2D, rpts2D,
			  lK, ldistCoeffs, rK, rdistCoeffs,
			  _imgSize, _rotMat, _transMat,
			  _essentialMat, _fundamentalMat,
			  cv::CALIB_FIX_INTRINSIC, term); 
    std::cout<<"\nStereocalibration error at stage 1: "<<rmsErr<<std::flush;
    
    // Recalibrate while improving camera intrinsics
    rmsErr =
      cv::stereoCalibrate(pts3D, lpts2D, rpts2D,
			  lK, ldistCoeffs, rK, rdistCoeffs,
			  _imgSize, _rotMat, _transMat,
			  _essentialMat, _fundamentalMat,
			  cv::CALIB_USE_INTRINSIC_GUESS, term);
    std::cout<<"\nStereocalibration error at stage 2: "<<rmsErr<<std::flush;

    // Compute projection matrices
    _lP = cv::Mat::zeros(3,4,CV_64F);
    _rP = cv::Mat::zeros(3,4,CV_64F);

    // Left projection matrix: lK[I|0]
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	_lP.at<double>(i,j) = lK.at<double>(i,j);

    // Right projection matrix: rK[R|t] = [ [rK x R]_{3x3} | [rK x t]_{3x1} ] <- 4x3 matrix.
    double val;
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  val = 0.;
	  for(int k=0; k<3; ++k)
	    val += rK.at<double>(i,k)*_rotMat.at<double>(k,j);
	  _rP.at<double>(i,j) = val;
	}

    for(int i=0; i<3; ++i)
      {
	val = 0.;
	for(int j=0; j<3; ++j)
	  val += rK.at<double>(i,j)*_transMat.at<double>(j,0);
	_rP.at<double>(i,3) = val;
      }
  }
}
