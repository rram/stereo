# Sriramajayam

add_library(${PROJECT_NAME}_srig STATIC
  str_StereoRig.cpp
  str_StereoRig_chess.cpp
  str_StereoRig_charuco.cpp)

# headers
target_include_directories(${PROJECT_NAME}_srig PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(${PROJECT_NAME}_srig PUBLIC ${PROJECT_NAME}_camera)

# Add required flags
target_compile_features(${PROJECT_NAME}_srig PUBLIC ${stereo_COMPILE_FEATURES})

# install header files
install(FILES
  str_StereoRig.h
  DESTINATION ${PROJECT_NAME}/include)
