// Sriramajayam

#include <str_StereoRig.h>
#include <iostream>

namespace str
{
  // Default constructor
  StereoRig::StereoRig()
    :_imgSize(0,0) {}

  
  // Constructor to read stereorig data from a file
  StereoRig::StereoRig(const cv::String xmlfile)
  { Create(xmlfile); }

  
  // Destructor
  StereoRig::~StereoRig() {}
  
  // Create from a file
  void StereoRig::Create(const cv::String xmlfile)
  {
    std::cout<<"\nCreating stereo rig from file "<<xmlfile<<std::flush;
    
    // Open file with parameters
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() &&
	      "str::StereoRig::Create()- Could not open file with stereo parameters.");

    // Read image size
    auto fn = fs["image_width"];
    CV_Assert(!fn.empty() &&
	      "str::StereoRig::Create()- Could not read image_width field.");
    cv::read(fn, _imgSize.width, 0);
    
    fn = fs["image_height"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read image_height field.");
    cv::read(fn, _imgSize.height, 0);
    
    CV_Assert((_imgSize.height>0 && _imgSize.width>0) &&
	      "str::StereoRig::Create()- Unexpected image sizes.");
    
    // Read data for left camera and create it
    fn = fs["lcamera_mat"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read lcamera_mat field.");
    cv::Mat lK, ldistCoeffs;
    cv::read(fn, lK);
    fn = fs["lcamera_distcoeffs"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read lcamera_distcoeffs field.");
    cv::read(fn, ldistCoeffs);
    _lcam.SetIntrinsics(lK, ldistCoeffs, _imgSize);
    CV_Assert(_lcam.IsCalibrated() && "str::StereoRig::Create()- left camera not calibrated.");
    
    // Read data for right camera and create it
    fn = fs["rcamera_mat"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read rcamera_mat field.");
    cv::Mat rK, rdistCoeffs;
    cv::read(fn, rK);
    fn = fs["rcamera_distcoeffs"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read rcamera_distcoeffs field.");
    cv::read(fn, rdistCoeffs);
    _rcam.SetIntrinsics(rK, rdistCoeffs, _imgSize);
    CV_Assert(_rcam.IsCalibrated() && "str::StereoRig::Create()- right camera not calibrated.");
    
    // Read rotation matrix
    fn = fs["rotation_mat"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read rotation_mat field.");
    cv::read(fn, _rotMat);
    
    // Read translation matrix
    fn = fs["translation_mat"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read translation_mat field.");
    cv::read(fn, _transMat);
    
    // Read essential matrix
    fn = fs["essential_mat"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read essential_mat field.");
    cv::read(fn, _essentialMat);
    
    // Read fundamental matrix
    fn = fs["fundamental_mat"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read fundamental_mat field.");
    cv::read(fn, _fundamentalMat);

    // Read left camera projection matrix
    fn = fs["lcamera_proj_mat"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read lcamera_proj_mat.");
    cv::read(fn, _lP);

    // Read right camera projection matrix
    fn = fs["rcamera_proj_mat"];
    CV_Assert(!fn.empty() && "str::StereoRig::Create()- Could not read rcamera_proj_mat.");
    cv::read(fn, _rP);
    
    // Finished reading
    fs.release();
  }


  // Copy constructor
  StereoRig::StereoRig(const StereoRig& obj)
    :_imgSize(obj._imgSize),
     _lcam(obj._lcam), _rcam(obj._rcam), 
     _rotMat(obj._rotMat),
     _transMat(obj._transMat),
     _essentialMat(obj._essentialMat),
     _fundamentalMat(obj._fundamentalMat),
     _lP(obj._lP), _rP(obj._rP) {}

  // Returns the image size
  const cv::Size& StereoRig::GetImageSize() const
  { return _imgSize; }
  
  // Returns camera matrix
  const Camera& StereoRig::GetLeftCamera() const
  { return _lcam; }

  const Camera& StereoRig::GetRightCamera() const
    { return _rcam; }
    
  // Matrices for the stereo-system
  const cv::Mat& StereoRig::GetFundamentalMatrix() const
  { return _fundamentalMat; }
  
  const cv::Mat& StereoRig::GetEssentialMatrix() const
    { return _essentialMat; }
    
  void StereoRig::GetCoordinateMap(cv::Mat& R,
				cv::Mat& T) const
  { R = _rotMat; T = _transMat; }
  
  void StereoRig::GetStereoMatrices(cv::Mat& R, cv::Mat& T,
				 cv::Mat& E, cv::Mat& F) const
  {
    R = _rotMat;
    T = _transMat;
    E = _essentialMat;
    F = _fundamentalMat;
  }
  
  
  // Saves stereo-rig details to a file
  void StereoRig::Save(const cv::String xmlfile) const
  {
    std::cout<<"\nSaving stereo rig to file "<<xmlfile<<std::flush;
    
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "str::StereoRig::Save()- Could not open file to write.");

    // Write stereo-matrices to file
    fs << "image_width" << _imgSize.width;
    fs << "image_height" << _imgSize.height;
    fs << "rotation_mat" << _rotMat;
    fs << "translation_mat" << _transMat;
    fs << "essential_mat" << _essentialMat;
    fs << "fundamental_mat" << _fundamentalMat;
    fs << "lcamera_mat" << _lcam.GetCameraMatrix();
    fs << "lcamera_distcoeffs" << _lcam.GetDistortionCoeffs();
    fs << "rcamera_mat" << _rcam.GetCameraMatrix();
    fs << "rcamera_distcoeffs" << _rcam.GetDistortionCoeffs();
    fs << "lcamera_proj_mat" << _lP;
    fs << "rcamera_proj_mat" << _rP;
    
    fs.release();
  }
  

  // Camera projection matrices
  void StereoRig::GetProjectionMatrices(cv::Mat& lP, cv::Mat& rP) const
  { lP = _lP; rP = _rP; return; }

  
  // Perform consistency checks on E, F, R, T
  // Checks det(E) = det(F) = 0, det(R) = 1,
  // relationship between E and F,
  // relationship between E, R and T.
  bool StereoRig::ConsistencyTest(const double EPS) const
  {
    const auto& lK = _lcam.GetCameraMatrix();
    const auto& rK = _rcam.GetCameraMatrix();
    const auto& E = _essentialMat;
    const auto& F = _fundamentalMat;
    const auto& R = _rotMat;
    const auto& T = _transMat;

    // Sizes and types
    CV_Assert(F.rows==3 && F.cols==3 && F.type()==CV_64F);
    CV_Assert(E.rows==3 && E.cols==3 && E.type()==CV_64F);
    CV_Assert(R.rows==3 && R.cols==3 && R.type()==CV_64F);
    CV_Assert(T.rows==3 && T.cols==1 && T.type()==CV_64F);
    
    // Determinants of E, F and R
    double det = cv::determinant(F);
    if(std::abs(det)>EPS)
      {
	std::cout<<"\nStereoRig::ConsistencyTest()- "
		 <<"\ndet(F) = "<<det<<" exceeded tolerance "<<EPS<<std::flush;
	return false;
      }
    
    det = cv::determinant(E);
    if(std::abs(det)>EPS)
      {
	std::cout<<"\nStereoRig::ConsistencyTest()- "
		 <<"\ndet(E) = "<<det<<" exceeded tolerance "<<EPS<<std::flush;
	return false;
      }

    det = cv::determinant(R);
    if(std::abs(det-1.)>EPS)
      {
	std::cout<<"\nStereoRig::ConsistencyTest()- "
		 <<"\ndet(R) = "<<det<<" exceeded 1 by tolerance "<<EPS<<std::flush;
	return false;
      }

    // Relationship between E and F: E = rK^T F lK.
    double val1, val2;
    cv::Mat Ep(3,3,CV_64F);
    int irow, icol;
    double maxval = E.at<double>(0,0);
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  auto& val = Ep.at<double>(i,j);
	  val = 0.;
	  for(int k=0; k<3; ++k)
	    for(int L=0; L<3; ++L)
	      val += rK.at<double>(k,i)*F.at<double>(k,L)*lK.at<double>(L,j);
	  
	  // Record the max. element in E and its location
	  if(std::abs(E.at<double>(i,j))>std::abs(maxval))
	    { irow = i; icol = j; maxval = E.at<double>(i,j); }
	}
    
    // E and Ep can differ by a scale factor.
    // They have to be normalized by corresponding matrix elements.
    double maxvalp = Ep.at<double>(irow, icol);
    CV_Assert( (std::abs(maxval)>EPS && std::abs(maxvalp)>EPS) &&
	       "str::StereoRig::ConsistencyTest()- E was close to zero.");
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  val1 = E.at<double>(i,j)/maxval;
	  val2 = Ep.at<double>(i,j)/maxvalp;
	  if(std::abs(val1-val2)>EPS)
	    {
	      std::cout<<"\nStereoRig::ConsistencyTest()- "
		       <<"Check for relation between essential and fundamental matrices failed: "
		       <<"\n"<<val1<<" should be close to "<<val2
		       <<" with tolerance "<<EPS<<std::flush;
	      return false;
	    }
	}
    
    // E should equal \hat{T} R
    double That[3][3] =
      {{0.,                -T.at<double>(2,0), T.at<double>(1,0)},
       {T.at<double>(2,0),  0.,                -T.at<double>(0,0)},
       {-T.at<double>(1,0), T.at<double>(0,0), 0.}};
    
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  val1 = E.at<double>(i,j);
	  val2 = 0.;
	  for(int k=0; k<3; ++k)
	    val2 += That[i][k]*R.at<double>(k,j);
	  
	  if(std::abs(val1-val2)>EPS)
	    {
	      std::cout<<"\nStereoRig::ConsistencyTest()- "
		       <<"Check for relation between essential and rotation/translation  matrices failed: "
		       <<"\n"<<val1<<" should be close to "<<val2
		       <<" with tolerance "<<EPS<<std::flush;
	      return false;
	    }
	}

    // Left projection matrix = lK[I|0]
    for(int i=0; i<3; ++i)
      {
	for(int j=0; j<3; ++j)
	  if(std::abs(lK.at<double>(i,j)-_lP.at<double>(i,j))>EPS)
	    {
	      std::cout<<"\nStereoRig::ConsistencyTest()- "
		       <<"Check for left projection matrix failed: "
		       <<"\n"<<_lP.at<double>(i,j)<<" should be close to "<<lK.at<double>(i,j)
		       <<" with tolerance "<<EPS;
	      std::fflush(stdout);
	      return false;
	    }
	
	// Last column
	if(std::abs(_lP.at<double>(i,3))>EPS)
	  {
	    std::cout<<"\nStereoRig::ConsistencyTest()- "
		     <<"Check for left projection matrix failed: "
		     <<"\n"<<_lP.at<double>(i,3)<<" should be close to 0."
		     <<" with tolerance "<<EPS;
	    std::fflush(stdout);
	    return false;
	  }
      }
    
    
    
    // Right projection matrix:
    double P[3][4];
    for(int i=0; i<3; ++i)
      {
	P[i][3] = 0.;
	for(int j=0; j<3; ++j)
	  {
	    P[i][3] += rK.at<double>(i,j)*T.at<double>(j,0);
	    P[i][j] = 0.;
	    for(int k=0; k<3; ++k)
	      P[i][j] += rK.at<double>(i,k)*R.at<double>(k,j);
	  }
      }
    for(int i=0; i<3; ++i)
      for(int j=0; j<4; ++j)
	if(std::abs(_rP.at<double>(i,j)-P[i][j])>EPS)
	  {
	    std::cout<<"\nStereoRig::ConsistencyTest()- "
		     <<"Check for right projection matrix failed: "
		     <<"\n"<<_rP.at<double>(i,j)<<" should be close to "<<P[i][j]
		     <<" with tolerance "<<EPS;
	    std::fflush(stdout);
	    return false;
	  }

    return true;
  }

}
