// Sriramajayam

#include <str_Triangulator.h>
#include <iostream>

namespace str
{
  // Implementation of unsymmetric option for triangulation
  template<>
  void Triangulator::Triangulate<TriMethod::Unsym>(const cv::Point2d& l, const cv::Point2d& r, cv::Point3d& X) const
  {
    // Calibrated coordinates
    double cl[] = {_lKinv.at<double>(0,0)*l.x + _lKinv.at<double>(0,1)*l.y + _lKinv.at<double>(0,2),
		   _lKinv.at<double>(1,0)*l.x + _lKinv.at<double>(1,1)*l.y + _lKinv.at<double>(1,2),
		   _lKinv.at<double>(2,0)*l.x + _lKinv.at<double>(2,1)*l.y + _lKinv.at<double>(2,2)};

    double cr[] = {_rKinv.at<double>(0,0)*r.x + _rKinv.at<double>(0,1)*r.y + _rKinv.at<double>(0,2),
		   _rKinv.at<double>(1,0)*r.x + _rKinv.at<double>(1,1)*r.y + _rKinv.at<double>(1,2),
		   _rKinv.at<double>(2,0)*r.x + _rKinv.at<double>(2,1)*r.y + _rKinv.at<double>(2,2)};

    // Normalize cl and cr to canonical form
    for(int i=0; i<3; ++i)
      { cl[i] /= cl[2]; cr[i] /= cr[2]; }
			 
    // R and T matrices
    cv::Mat R, T;
    _rig.GetCoordinateMap(R, T);
    
    // R x cl
    double Rcl[] = {0.,0.,0.};
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	Rcl[i] += R.at<double>(i,j)*cl[j];
    
    // Solve the equation lambda(cr x Rcl) + cr x T = 0,
    // where the scale factor for the translation has been set to 1.
    double lvec[3], rvec[3];
    int j, k;
    for(int i=0; i<3; ++i)
      {
	j = (i+1)%3;
	k = (i+2)%3;
	lvec[i] = cr[j]*Rcl[k] - cr[k]*Rcl[j];
	rvec[i] = cr[j]*T.at<double>(k,0) - cr[k]*T.at<double>(j,0);
      }
    double lhs = lvec[0]*lvec[0] + lvec[1]*lvec[1] + lvec[2]*lvec[2];
    CV_Assert(lhs>1.e-6 && "str::Triangulator::Triangulate<Unsym>()- attempting to divide by a small number.");
    double rhs = lvec[0]*rvec[0] + lvec[1]*rvec[1] + lvec[2]*rvec[2];
    double lambda = -rhs/lhs;

    X.x = lambda*cl[0];
    X.y = lambda*cl[1];
    X.z = lambda*cl[2];
    return;
  }

  
  // Implementation of symmetric option for triangulator
  template<>  void Triangulator::
  Triangulate<TriMethod::Sym>(const cv::Point2d& l, const cv::Point2d& r, cv::Point3d& X) const
  {
    // Calibrated coordinates
    double cl[] = {_lKinv.at<double>(0,0)*l.x + _lKinv.at<double>(0,1)*l.y + _lKinv.at<double>(0,2),
		   _lKinv.at<double>(1,0)*l.x + _lKinv.at<double>(1,1)*l.y + _lKinv.at<double>(1,2),
		   _lKinv.at<double>(2,0)*l.x + _lKinv.at<double>(2,1)*l.y + _lKinv.at<double>(2,2)};

    double cr[] = {_rKinv.at<double>(0,0)*r.x + _rKinv.at<double>(0,1)*r.y + _rKinv.at<double>(0,2),
		   _rKinv.at<double>(1,0)*r.x + _rKinv.at<double>(1,1)*r.y + _rKinv.at<double>(1,2),
		   _rKinv.at<double>(2,0)*r.x + _rKinv.at<double>(2,1)*r.y + _rKinv.at<double>(2,2)};

    // Normalize cl and cr to canonical form
    for(int i=0; i<3; ++i)
      { cl[i] /= cl[2]; cr[i] /= cr[2]; }
			 
    // R and T matrices
    cv::Mat R, T;
    _rig.GetCoordinateMap(R, T);
    
    // R x cl
    double Rcl[] = {0.,0.,0.};
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	Rcl[i] += R.at<double>(i,j)*cl[j];

    // Solve the system lambda2 cr = lambda1 R cl + T.
    cv::Mat_<double> A(3,2);
    for(unsigned int i=0; i<3; ++i)
      {
	A.at<double>(i,0) = -Rcl[i];
	A.at<double>(i,1) = cr[i];
      }

    // Find a nontrivial solution
    cv::Mat sol;
    const auto flag = cv::solve(A, T, sol, cv::DECOMP_NORMAL);
    CV_Assert(flag==true && "str::Triangulator::Triangulate()- could not solve over-determined system.");
    CV_Assert((sol.cols==1 && sol.rows==2) && "str::Triangulator::Triangulate()- unexpected solution.");

    const double lambda1 = sol.at<double>(0,0);
    const double lambda2 = sol.at<double>(1,0);
    cv::Mat Rinv = R.inv();
    double Y[3] = {0.,0.,0.};
    for(unsigned int i=0; i<3; ++i)
      for(unsigned int j=0; j<3; ++j)
	Y[i] += Rinv.at<double>(i,j)*(lambda2*cr[j]-T.at<double>(j,0));
    
    //std::cout<<"\nLambda1: "<<lambda1<<", lambda2: "<<lambda2; std::fflush( stdout );
    //std::cout<<"\nSolution: "<<sol; std::fflush( stdout );

    // Return the average of the found points lambda1 x1 and lambda2 x2
    const double alpha = 0.5;
    X.x = alpha*lambda1*cl[0]+(1.-alpha)*Y[0];
    X.y = alpha*lambda1*cl[1]+(1.-alpha)*Y[1];
    X.z = alpha*lambda1*cl[2]+(1.-alpha)*Y[2];
    return;
  }

}
    
/*
  
// Implementation of symmetric option for triangulator
template<>  void Triangulator::
Triangulate<TriMethod::Sym>(const cv::Point2d& l, const cv::Point2d& r, cv::Point3d& X) const
{
// Camera projection matrices
cv::Mat lP, rP;
_rig.GetProjectionMatrices(lP, rP);
    
// Assemble the matrix A so identify the null vector AX=0
cv::Mat_<double> A(4,3);
for(int j=0; j<3; ++j)
{
// Row 0
A(0,j) = l.x*lP.at<double>(2,j)-lP.at<double>(0,j);
// Row 1
A(1,j) = l.y*lP.at<double>(2,j)-lP.at<double>(1,j);
// Row 2
A(2,j) = r.x*rP.at<double>(2,j)-rP.at<double>(0,j);
// Row 3
A(3,j) = r.y*rP.at<double>(2,j)-rP.at<double>(1,j);
}

// Find a nontrivial solution
cv::Mat sol;
cv::SVD::solveZ(A, sol);
CV_Assert((sol.cols==1 && sol.rows==3) && "\nvc::Triangulator::Triangulate()- unexpected solution.\n");
    
// The solution is normalized to length 1.
// Compute the scaling factor: x = P(lambda X) -> x.x = lambda(x.PX)
double lhs = l.x*l.x + l.y*l.y;
double PX[] = {lP.at<double>(0,0)*sol.at<double>(0,0)+lP.at<double>(0,1)*sol.at<double>(1,0)+lP.at<double>(0,2)*sol.at<double>(2,0)+lP.at<double>(0,3),
lP.at<double>(1,0)*sol.at<double>(0,0)+lP.at<double>(1,1)*sol.at<double>(1,0)+lP.at<double>(1,2)*sol.at<double>(2,0)+lP.at<double>(1,3),
lP.at<double>(2,0)*sol.at<double>(0,0)+lP.at<double>(2,1)*sol.at<double>(1,0)+lP.at<double>(2,2)*sol.at<double>(2,0)+lP.at<double>(2,3)};
for(int i=0; i<3; ++i)
PX[i] /= PX[2];
    
double rhs = l.x*PX[0] + l.y*PX[1];
double lambda = lhs/rhs;
lambda = 1.;
X.x = lambda*sol.at<double>(0,0);
X.y = lambda*sol.at<double>(1,0);
X.z = lambda*sol.at<double>(2,0);
return;
}*/
