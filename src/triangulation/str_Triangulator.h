// Sriramajayam

#ifndef STR_TRIANGULATOR_H
#define STR_TRIANGULATOR_H

#include <str_StereoRig.h>
#include <fstream>

namespace str
{
  enum class TriMethod {Unsym, Sym, Sampson, Optimal};
  
  class Triangulator
  {
  public:

    // Constructor
    inline Triangulator(const StereoRig& rig)
      :_rig(rig)
    {
      double flag = cv::invert(_rig.GetLeftCamera().GetCameraMatrix(), _lKinv, cv::DECOMP_LU);
      CV_Assert(flag>0 && "str::Triangulator::Triangulator()- could not invert left cam matrix.");
      flag = cv::invert(_rig.GetRightCamera().GetCameraMatrix(), _rKinv, cv::DECOMP_LU);
      CV_Assert(flag>0 && "str::Triangulator::Triangulator()- could not invert right cam matrix.");
    }

    // Destructor, does nothing
    inline virtual ~Triangulator() {}

    // Copy constructor
    inline Triangulator(const Triangulator& obj)
      :_rig(obj._rig),
      _lKinv(obj._lKinv),
      _rKinv(obj._rKinv) {}

    // Returns the stereo rig
    inline const StereoRig& GetStereoRig() const
    { return _rig; }

    // Main functionality:
    // triangulates points with given pixel coordinates
    // according to one of requested methods
    // Specialized implementations to be provided.
    template<TriMethod tm>
      void Triangulate(const cv::Point2d& xl, const cv::Point2d& xr, cv::Point3d& X) const;

    // Main functionality: triangulates points read from a file with the specified method
    template<TriMethod tm>
      void Triangulate(const cv::String xmlfile) const;
    
  private:
    const StereoRig& _rig; //!< Reference to stereo-rig
    cv::Mat _lKinv, _rKinv;  //!< Pre-compute the left and right camera matrix inverses
  }; 
}

#include <str_Triangulator_triangulate.h>

#endif
