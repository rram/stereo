// Sriramajayam

#ifndef STR_TRIANGULATOR_IMPL_H
#define STR_TRIANGULATOR_IMPL_H

namespace str
{
  // Main functionality: triangulates points read from a file with the specified method
  template<TriMethod tm>
    void Triangulator::Triangulate(const cv::String xmlfile) const
    {
      // Open the given xml file
      cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
      CV_Assert(fs.isOpened() && "str::Triangulator::Triangulate()- Could not open file.");

      // read points
      cv::Mat lMat, rMat;
      auto fn = fs["pixel_set_1"];
      CV_Assert(!fn.empty() && "str::Triangulator::Triangulate()- could not read field pixel_set_1.");
      cv::read(fn, lMat);
      
      fn = fs["pixel_set_2"];
      CV_Assert(!fn.empty() && "str::Triangulator::Triangulate()- could not read field pixel_set_2.");
      cv::read(fn, rMat);
      fs.release();
      
      // Check that both matrices have identical size, >=2 columns and one channel
      CV_Assert(lMat.size()==rMat.size() &&
		"str::Triangulator::Triangulate()- inconsistent point set sizes.");
      CV_Assert((lMat.cols==2 && rMat.cols==lMat.cols) &&
		"str::Triangulator::Triangulate()- inconsistent number of columns in point set data.");
      CV_Assert((lMat.channels()==1 && rMat.channels()==1) &&
		"str::Triangulator::Triangulate()- inconsistent number of channels in point set data.");
      const int nPoints = lMat.rows;

      // Matrix of the number of points
      cv::Mat_<double> pMat(nPoints, 3);
      cv::Point2d lPoint, rPoint;
      cv::Point3d X;
      for(int p=0; p<nPoints; ++p)
	{
	  // Read this point from left and right cameras
	  lPoint.x = lMat.at<double>(p,0); lPoint.y = lMat.at<double>(p,1);
	  rPoint.x = rMat.at<double>(p,0); rPoint.y = rMat.at<double>(p,1);
	  
	  // Triangulate this point
	  Triangulate<tm>(lPoint, rPoint, X);
	  pMat(p,0) = X.x; pMat(p,1) = X.y; pMat(p,2) = X.z;
	}

      // Append xyz coordinates to the XML file
      cv::FileStorage pfile(xmlfile, cv::FileStorage::APPEND);
      CV_Assert(pfile.isOpened() && "str::Triangulator::Triangulator()- Could not open file for appending.");
      pfile.writeComment((char*)"Triangulated point set", 0);
      pfile << "point_set" << pMat;
      pfile.release();
      return;
    }
}

#endif
