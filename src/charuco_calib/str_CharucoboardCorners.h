// Sriramajayam

#ifndef STR_CHARUCOBOARD_CORNERS_H
#define STR_CHARUCOBOARD_CORNERS_H

#include <vector>
#include <opencv2/core.hpp>

namespace str
{
  class CharucoboardCorners
  {
  public:
    //! Default constructor
    inline CharucoboardCorners()
      :nImages(0), cornerIDs({}), corners3D({}),
      corners2D({}), rvecs({}), tvecs({}) {}

    //! Copy constructor
    inline CharucoboardCorners(const CharucoboardCorners& Obj)
      :nImages(Obj.nImages),
      cornerIDs(Obj.cornerIDs),
      corners3D(Obj.corners3D),
      corners2D(Obj.corners2D),
      rvecs(Obj.rvecs),
      tvecs(Obj.tvecs) {}

    //! Match: Helper function that given two boards, returns 2 new boards having
    //! overlapping markers in respective images
    static void Match(const CharucoboardCorners& A, const CharucoboardCorners& B,
		      CharucoboardCorners& newA, CharucoboardCorners& newB);

    //! Sanity checks
    void Check() const;

    //! Clear all contents
    void Clear();
    
    // Members
    int nImages;
    std::vector<std::vector<int>> cornerIDs;
    std::vector<std::vector<cv::Point3f>> corners3D;
    std::vector<std::vector<cv::Point2f>> corners2D;
    std::vector<cv::Mat> rvecs, tvecs;
  };
}

#endif
