// Sriramajayam

#include <str_Charucoboard.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <iostream>
#include <random>
#include <algorithm>
#include <set>
#include <array>

namespace str
{
  const std::map<cv::String, cv::aruco::PredefinedDictionaryType>
  Charucoboard::_dictionaryMap
  = {{"DICT_4X4_100", cv::aruco::DICT_4X4_100},
     {"DICT_4X4_250", cv::aruco::DICT_4X4_250},
     {"DICT_4X4_1000", cv::aruco::DICT_4X4_1000},
     {"DICT_5X5_50", cv::aruco:: DICT_5X5_50},
     {"DICT_5X5_100", cv::aruco::DICT_5X5_100},
     {"DICT_5X5_250", cv::aruco::DICT_5X5_250},
     {"DICT_5X5_1000", cv::aruco::DICT_5X5_1000}, 
     {"DICT_6X6_50", cv::aruco::DICT_6X6_50},  
     {"DICT_6X6_100", cv::aruco::DICT_6X6_100},  
     {"DICT_6X6_250", cv::aruco::DICT_6X6_250},
     {"DICT_6X6_1000", cv::aruco::DICT_6X6_1000},
     {"DICT_7X7_50", cv::aruco::DICT_7X7_50},
     {"DICT_7X7_100", cv::aruco::DICT_7X7_100},
     {"DICT_7X7_250", cv::aruco::DICT_7X7_250},
     {"DICT_7X7_1000", cv::aruco::DICT_7X7_1000}};
  
  // Create board from parameters specified in a file
  void Charucoboard::Create(const cv::String xmlfile)
  {
    std::cout<<"\nCreating Charucoboard from file "<<xmlfile<<std::flush;

    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() && "str::Charucoboard::Create()- Could not open file for construction.");

    // Read the elements
    auto fn = fs["squaresX"];
    CV_Assert(!fn.empty() && "str::Charucoboard::Create()- Could not read squaresX.");
    int squaresX = 0;
    cv::read(fn, squaresX, 0);

    fn = fs["squaresY"];
    CV_Assert(!fn.empty() && "str::Charucoboard::Create()- Could not read squaresY.");
    int squaresY = 0;
    cv::read(fn, squaresY, 0);

    fn = fs["squareLength"];
    CV_Assert(!fn.empty() && "str::Charucoboard::Create()- Could not read squareLength.");
    float squareLength = 0;
    cv::read(fn, squareLength, 0);
    
    fn = fs["markerLength"];
    CV_Assert(!fn.empty() && "str::Charucoboard::Create()- Could not read markerLength.");
    float markerLength = 0;
    cv::read(fn, markerLength, 0);

    fn = fs["markerBits"];
    CV_Assert(!fn.empty() && "str::Charucoboard::Create()- Could not read markerBits.");
    int markerBits;
    cv::read(fn, markerBits, 0);
    CV_Assert((markerBits==4 || markerBits==5 || markerBits==6 || markerBits==7) &&
	      "str::Charucoboard::Create()- markerBits should be 4, 5, 6 or 7.");

    // Compute the minimum number of markers required based on the size of the chessboard
    int nMinMarkers = (squaresX*squaresY)/2;
    nMinMarkers += 2; // Accounts for the odd case, safety factor of 1.

    // Has the number of markers been specified?
    int numMarkers = 0;
    fn = fs["numMarkers"];
    if(!fn.empty())
      {
	cv::read(fn, numMarkers, 0);
	CV_Assert((numMarkers==50 || numMarkers==100 ||
		   numMarkers==250 || numMarkers==1000) &&
		  "str::Charucoboard::Create()- numMarkers can be 50, 100, 250 or 1000.");
	CV_Assert(numMarkers>=nMinMarkers &&
		  "str::CharucoBoard::Create()- numMarkers is smaller than min. required.");
      }
    else
      {
	// Select the appropriate board
	if(nMinMarkers<50)
	  numMarkers = 50;
	else if(nMinMarkers<100)
	  numMarkers = 100;
	else if(nMinMarkers<250)
	  numMarkers = 250;
	else if(nMinMarkers<1000)
	  numMarkers = 1000;
	else
	  CV_Assert(false && "str::Charucoboard::Create()- number of markers required exceeds 1000.");
      }

    
    // Check options read from the file
    CV_Assert(squaresX>0 && "str::Charucoboard::Create()- unexpected value for squaresX");
    CV_Assert(squaresY>0 && "str::Charucoboard::Create()- unexpected value for squaresY");
    CV_Assert(squareLength>0 && "str::Charucoboard::Create()- unexpected value for squareLength");
    CV_Assert(markerLength>0 && "str::Charucoboard::Create()- unexpected value for markerLength");
    CV_Assert(squareLength>markerLength && "str::Charucoboard::Create()- square length should be larger than marker length.");
    CV_Assert(markerBits<markerLength && "str::Charucoboard::Create()- markerLength should exceed markerBits");
    
    // Create a dictionary name
    cv::String dictName =
      cv::String("DICT_") +
      cv::String(std::to_string(markerBits)) +
      cv::String("X") +
      cv::String(std::to_string(markerBits)) +
      cv::String("_")+
      cv::String(std::to_string(numMarkers));
    
    // Initialize the dictionary
    std::cout<<"Choosing dictionary "<<dictName<<" to create Charucoboard."<<std::flush;
    auto it = _dictionaryMap.find(dictName);
    CV_Assert(it!=_dictionaryMap.end() && "str::Charucoboard::Create()- unexpected dictionary name encountered.");
    dictionary = new cv::aruco::Dictionary(cv::aruco::getPredefinedDictionary(it->second));

    // Check dictionary
    CV_Assert(markerBits==dictionary->markerSize && "str::Charucoboard::Create()- incorrect number of bits in dictionary.");
    CV_Assert(numMarkers==dictionary->bytesList.rows && "str::Charucoboard::Create()- incorrect number of markers in dictionary.");

    // Create the board
    _board = new cv::aruco::CharucoBoard(cv::Size(squaresX, squaresY), squareLength, markerLength, *dictionary);
    _board->setLegacyPattern(true);
  }


  // Draw the board
  void Charucoboard::Draw(const cv::String filename,
			  const int marginSize, const int borderBits)
  {
    // Chessboard size
    auto cbSize = _board->getChessboardSize();
    auto squareLength = _board->getSquareLength();
    
    // Compute the required image size
    cv::Size imgSize(cbSize.width*squareLength+2*marginSize,
		     cbSize.height*squareLength+2*marginSize);

    // Draw
    cv::Mat img;
    cv::aruco::drawPlanarBoard(_board, imgSize, img, marginSize, borderBits);
    
    // Print the image
    std::cout<<"\nSaving charucoboard image to file "<<filename<<std::flush;
    cv::imwrite(filename, img);
  }


  // Save a board to as an xml file
  void Charucoboard::Save(const cv::String xmlfile) const
  {
    std::cout<<"\nSaving charucoboard to file "<<xmlfile<<std::flush;
    
    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "str::Charucoboard::Save()- Could not open file for construction.");

    // Chessboard size
    auto cbSize = _board->getChessboardSize();
    fs.writeComment("Number of squares in the X direction", 0);
    fs << "squaresX"<<cbSize.width;

    fs.writeComment("Number of squares in the Y direction", 0);
    fs << "squaresY"<<cbSize.height;

    // Square and marker length
    fs.writeComment((char*)"Length of each square (in pixels)", 0);
    fs << "squareLength" << _board->getSquareLength();
    fs.writeComment((char*)"Length of each marker (in pixels, smaller than squareLength", 0);
    fs << "markerLength" << _board->getMarkerLength();

    // Dictionary details
    fs.writeComment((char*)"Number of marker bits desired: 4,5,6 or 7 only", 0);
    fs << "markerBits" << _board->getDictionary().markerSize;
    fs.writeComment((char*)"Optional: Number of markers in dictionary. If not specified, a minimum number will be calculated. Rounded up to 50, 100, 250 or 1000", 0);
    fs << "numMarkers" << _board->getDictionary().bytesList.rows;
    fs.release();
  }

  // Main functionality: detect corners in an image
  void Charucoboard::Detect(CharucoBoardImage& cbImage) const
  {
    CV_Assert(!cbImage.imgName.empty() && "str::CharucoBoard::Detect()- Image name not provided.");
    
    // Read this image
    cv::Mat img = cv::imread(cbImage.imgName, cv::IMREAD_GRAYSCALE);
    CV_Assert(!img.empty() && "str::CharucoBoard::Detect()- Could not open image.");

    auto& nMarkers = cbImage.nMarkers;
    auto& nCorners = cbImage.nCorners;
    auto& markerIDs = cbImage.markerIDs;
    auto& markerCorners = cbImage.markerCorners;
    auto& charucoIDs = cbImage.charucoIDs;
    auto& charucoCorners = cbImage.charucoCorners;

    // Detect markers
    cv::Ptr<cv::aruco::DetectorParameters> detectorParams(new cv::aruco::DetectorParameters());
    detectorParams->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
    detectorParams->adaptiveThreshWinSizeMax = 100;
    
    std::vector<std::vector<cv::Point2f>> rejected;
    markerIDs.clear();
    markerCorners.clear();
    cv::aruco::detectMarkers(img, dictionary, markerCorners, markerIDs, detectorParams, rejected);

    // refind strategy to detect more markers
    cv::Ptr<cv::aruco::Board> arBoard = _board.staticCast<cv::aruco::Board>();
    cv::aruco::refineDetectedMarkers(img, arBoard, markerCorners, markerIDs, rejected);
    
    // Sanity check on number of markers
    nMarkers = static_cast<int>(markerIDs.size());
    CV_Assert(static_cast<int>(markerCorners.size())==nMarkers && "str::CharucoBoard::Detect()- Unexpected number of markers found.");
    CV_Assert(nMarkers>0 && "str::CharucoBoard::Detect()- No markers found.");
    CV_Assert(nMarkers<=static_cast<int>(_board->getIds().size()) && "str::CharucoBoard::Detect()- Found too many markers.");
    
    // Find corners
    charucoIDs.release();
    charucoCorners.release();
    nCorners = cv::aruco::interpolateCornersCharuco(markerCorners, markerIDs, img, _board, charucoCorners, charucoIDs);
    CV_Assert(nCorners>0 && "str::CharucoBoard::Detect()- Did not find any corners.");
    CV_Assert((charucoIDs.rows==nCorners && charucoCorners.rows==nCorners) &&
	      "str::CharucoBoard::Detect()- Inconsistent number of corners found.");
    auto cbsize = _board->getChessboardSize();
    CV_Assert(nCorners<=(cbsize.width-1)*(cbsize.height-1) && "str::CharucoBoard::Detect()- Found too many corners.");

    // Visualize identification
    cv::cvtColor(img, img, cv::COLOR_GRAY2BGR);
    cv::Vec3b blue(255,0,0);
    cv::Vec3b red(0,0,255);

    // Markers in blue
    for(int m=0; m<nMarkers; ++m)
      for(int j=0; j<4; ++j)
	cv::circle(img, cv::Point(markerCorners[m][j].x, markerCorners[m][j].y), 5, blue, cv::FILLED);

    // Corners in red
    cv::aruco::drawDetectedCornersCharuco(img, charucoCorners, charucoIDs, red);

    // Save visualization
    auto pos = cbImage.imgName.find_last_of((char*)"/");
    cv::String filename = cv::String("detected_") + cv::String(cbImage.imgName, pos+1);
    std::cout<<"\nVisualization of detected corners in "<<cbImage.imgName<<" saved in "<<filename<<std::flush;
    cv::imwrite(filename, img);
  }

    
  // Compute corresponding corners in a pair of images
  void Charucoboard::MatchImages(const cv::String img1Name, const cv::String img2Name,
				 cv::String ptsfile) const
  {
    CharucoBoardImage cb1, cb2;
    Detect(img1Name, cb1);
    Detect(img2Name, cb2);
    
    // Convert corner IDs from Mat to std::vector
    std::vector<int> id1(cb1.nCorners);
    for(int i=0; i<cb1.nCorners; ++i) id1[i] = cb1.charucoIDs.at<int>(i,0);
    std::vector<int> id2(cb2.nCorners);
    for(int i=0; i<cb2.nCorners; ++i) id2[i] = cb2.charucoIDs.at<int>(i,0);
      
    // Corners with matching indices in both images
    std::vector<int> matchCornerIDs;
    std::set_intersection(id1.begin(), id1.end(), id2.begin(), id2.end(),
			  std::back_inserter(matchCornerIDs));
    const int nMatchCorners = static_cast<int>(matchCornerIDs.size());
    CV_Assert(nMatchCorners>0 && "str::Charucoboard::Match()- no matching corners found.");
    std::cout<<"\nNumber of matched corners: "<<nMatchCorners<<std::flush;
      
    // Invert map from index to IDs.
    std::map<int, int> inv_id1, inv_id2;
    for(int i=0; i<cb1.nCorners; ++i) inv_id1[id1[i]] = i;
    for(int i=0; i<cb2.nCorners; ++i) inv_id2[id2[i]] = i;

    // Look up coordinates of corners identified in both images
    cv::Mat_<double> leftCorresp(nMatchCorners, 2);
    cv::Mat_<double> rightCorresp(nMatchCorners, 2);
    cv::Mat_<int> matchID(nMatchCorners, 1);
    for(int i=0; i<nMatchCorners; ++i)
      {
	auto& id = matchCornerIDs[i];
	auto it1 = inv_id1.find(id);
	auto it2 = inv_id2.find(id);
	CV_Assert((it1!=inv_id1.end() && it2!=inv_id2.end()) && "str::Charucoboard::Match()- inconsistent inverse index map.");

	// Note down pixel coordinates from the two images
	auto& corner1 = cb1.charucoCorners.at<cv::Point2f>(it1->second, 0);
	leftCorresp(i,0) = corner1.x;
	leftCorresp(i,1) = corner1.y;
	  
	auto& corner2 = cb2.charucoCorners.at<cv::Point2f>(it2->second, 0);
	rightCorresp(i,0) = corner2.x;
	rightCorresp(i,1) = corner2.y;
	  
	matchID(i,0) = id;
      }

    // Visualize the corresponding set of corners
    cv::Mat img1 = cv::imread(cb1.imgName, cv::IMREAD_COLOR);
    cv::Mat img2 = cv::imread(cb2.imgName, cv::IMREAD_COLOR);

    // Random color generation
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(1, 255);
    cv::Vec3b color;

    const int rad = 10;
    cv::Vec3b red(0,0,255);
    cv::Vec3b yellow(0,255,255);
    cv::Point2f pt;
    cv::Point2f offset(rad,rad);
    for(int i=0; i<nMatchCorners; ++i)
      {
	// text to print
	//cv::String txt = cv::String("cnr ")+cv::String(std::to_string(matchID(i,0)).c_str());
	cv::String txt = cv::String(" ")+cv::String(std::to_string(matchID(i,0)).c_str());

	// color of dot
	color.val[0] = dist(gen);
	color.val[1] = dist(gen);
	color.val[2] = dist(gen);

	// Image 1
	pt.x = leftCorresp(i,0);
	pt.y = leftCorresp(i,1);
	cv::circle(img1, pt, rad, color, cv::FILLED);
	putText(img1, txt, pt+offset, cv::FONT_HERSHEY_COMPLEX, 1.25, yellow, 2);

	// Image 2
	pt.x = rightCorresp(i,0);
	pt.y = rightCorresp(i,1);
	cv::circle(img2, pt, rad, color, cv::FILLED);
	putText(img2, txt, pt+offset, cv::FONT_HERSHEY_COMPLEX, 1.25, yellow, 2);
      }
      
    auto pos = cb1.imgName.find_last_of((char*)"/");
    cv::String filename = "lmatch_" + cv::String(cb1.imgName, pos+1);
    std::cout<<"\nMatched corners in image "<<cb1.imgName<<" visualized in "<<filename<<std::flush;
    cv::imwrite(filename, img1);

    pos = cb2.imgName.find_last_of((char*)"/");
    filename = "rmatch_" + cv::String(cb2.imgName, pos+1);
    std::cout<<"\nMatched corners in image "<<cb2.imgName<<" visualized in "<<filename<<std::flush;
    cv::imwrite(filename, img2);

    // Save the identified pixels and IDs in an xml file
    cv::FileStorage fs(ptsfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "str::Charucoboard::Match()- Could not open pixels file for writing.");
    fs.writeComment((char*)"Pixel set 1", 0);
    fs << "pixel_set_1" << leftCorresp;
    fs.writeComment((char*)"Pixel set in scene 2", 0);
    fs << "pixel_set_2" << rightCorresp;
    fs.writeComment((char*)"IDs of corners for matching pixel sets", 0);
    fs << "corner_ids" << matchID;
    fs.release();
    return;
  }


  // Anonymous namespace
  namespace
  {
    // Helper function to read pixels and corner ids
    void ReadCorners(const cv::String xmlfile, cv::Mat_<double>& left, cv::Mat_<double>& right,
		     cv::Mat_<int>& ids)
    {
      // Read the sets of matching corners and their IDs from the pair of xml files
      cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
      CV_Assert(fs.isOpened() && "str::Charucoboard::ReadCorners()- Could not open file for reading.");
      auto fn = fs["pixel_set_1"];
      CV_Assert(!fn.empty() && "str::Charucoboard::ReadCorners()- Could not read pixels for corners.");
      fn >> left;
      fn = fs["pixel_set_2"];
      CV_Assert(!fn.empty() && "str::Charucoboard::ReadCorners()- Could not read pixels for corners.");
      fn >> right;
      fn = fs["corner_ids"];
      CV_Assert(!fn.empty() && "str::Charucoboard::ReadCorners()- Could not read pixel ids for corners.");
      fn >> ids;
      fs.release();
      
      // Check dimensions
      CV_Assert((left.size()==right.size() && left.rows==ids.rows && ids.cols==1 && left.cols==2) &&
		"str::Charucoboard::ReadCorners()- unexpected pixel data format.");
      return;
    }

    // Identify common list of ids
    void IdentifyCommonCorners(const cv::Mat_<int>& A_ids, const cv::Mat_<int>& B_ids,
			       std::set<int>& commonIDs)
    {
      std::set<int> A_idset, B_idset;
      for(int i=0; i<A_ids.rows; ++i)
	A_idset.insert( A_ids(i,0) );
      for(int i=0; i<B_ids.rows; ++i)
	B_idset.insert( B_ids(i,0) );

      // Common ids
      commonIDs.clear();
      std::set_intersection(A_idset.begin(), A_idset.end(), B_idset.begin(), B_idset.end(),
			    std::inserter(commonIDs, commonIDs.begin()));
      return;
    }

    
    // Print a given set of corners to an xml file
    void PrintCommonCorners(const cv::Mat_<double>& left, const cv::Mat_<double>& right,
			    const cv::Mat_<int> ids, const std::set<int>& idsubset,
			    const cv::String outxml)
    {
      // Decide which rows to print
      std::map<int, int> ids2rowMap;
      for(int i=0; i<ids.rows; ++i)
	ids2rowMap[ids(i,0)] = i;

      std::vector<int> rownums; rownums.clear();
      for(auto& v:idsubset)
	{
	  auto it = ids2rowMap.find(v);
	  CV_Assert(it!=ids2rowMap.end() && "str::MatchPoses- Unexpected corner id encountered");
	  rownums.push_back(it->second);
	}

      // Subset of left & right pixel values
      cv::Mat_<double> leftsubset, rightsubset;
      for(int& r:rownums)
	{
	  leftsubset.push_back( left.row(r) );
	  rightsubset.push_back( right.row(r) );
	}

      int nrows = static_cast<int>(idsubset.size());
      cv::Mat_<int> idsubsetMat(nrows, 1);
      {
	int i=0;
	for(auto& v:idsubset)
	  idsubsetMat(i++,0) = v;
      }

      // Save the identified pixels and IDs in an xml file
      cv::FileStorage fs(outxml, cv::FileStorage::WRITE);
      CV_Assert(fs.isOpened() && "str::Charucoboard::MatchPoses- Could not open pixels file for writing.");
      fs.writeComment((char*)"Pixel set 1", 0);
      fs << "pixel_set_1" << leftsubset;
      fs.writeComment((char*)"Pixel set in scene 2", 0);
      fs << "pixel_set_2" << rightsubset;
      fs.writeComment((char*)"IDs of corners for matching pixel sets", 0);
      fs << "corner_ids" << idsubsetMat;
      fs.release();
    }
    
  }
  
  // Compute corresponding corners in a pair of poses
  void Charucoboard::MatchPoses(const cv::String pose1xml, const cv::String pose2xml,
				cv::String pose1subxml, cv::String pose2subxml) const
  {
    // Read corners from pose 1
    cv::Mat_<double> p1_left, p1_right;
    cv::Mat_<int> p1_ids;
    ReadCorners(pose1xml, p1_left, p1_right, p1_ids);

    // Read corners from pose 2
    cv::Mat_<double> p2_left, p2_right;
    cv::Mat_<int> p2_ids;
    ReadCorners(pose2xml, p2_left, p2_right, p2_ids);

    // Get the set of common corner ids between the two poses
    std::set<int> commonIDs;
    IdentifyCommonCorners(p1_ids, p2_ids, commonIDs);
    std::cout<<"\nstr::MatchPoses: number of common pose corners: "<<commonIDs.size()<<std::flush;
    
    // Print the pixel coordinates of common set of common corners from poses 1 and 2 into xml files
    PrintCommonCorners(p1_left, p1_right, p1_ids, commonIDs, pose1subxml);
    PrintCommonCorners(p2_left, p2_right, p2_ids, commonIDs, pose2subxml);

    return;
  }
  
  

  // namespace
  // {
  //   // Compute 3D coordinates of corners with given set of IDs
  //   void ComputeCorners(const cv::Mat_<double>& left, const cv::Mat_<double>& right,
  // 			const cv::Mat_<int>& ids, const std::set<int>& commonIDs,
  // 			const Triangulator& tri, const TriMethod method,
  // 			cv::Mat_<double>& cnrCoords)
  //   {
  //     // Inverse map from IDs to row # 
  //     std::map<int, int> id2rowMap;
  //     for(int row=0; row<ids.rows; ++row)
  // 	id2rowMap[ids(row,0)] = row;

  //     // Which function to invoke for triangulation
  //     //std::function<emplate<TriMethod tm>
  //     //void Triangulate(const cv::Point2d& xl, const cv::Point2d& xr, cv::Point3d& X) const;
      
  //     // Loop over common ids.
  //     // Identify its pixel coordinates in the left and right images
  //     // Triangulate. Save the coordinates.
  //     cv::Point3d X;
  //     cv::Mat_<double> XMat(1,3);
  //     cv::Point2d leftpx, rightpx;
  //     for(auto id:commonIDs)
  // 	{
  // 	  auto it = id2rowMap.find(id);
  // 	  CV_Assert(it!=id2rowMap.end());
  // 	  auto& row = it->second;
  // 	  leftpx.x = left(row, 0); leftpx.y = left(row, 1);
  // 	  rightpx.x = right(row, 0); rightpx.y = right(row, 1);

  // 	  // Triangulate
  // 	  tri.Triangulate<TriMethod::Sym>(leftpx, rightpx, X);
  // 	  XMat(0,0) = X.x; XMat(0,1) = X.y; XMat(0,2) = X.z;
  // 	  cnrCoords.push_back(XMat);
  // 	}
  //     return;
  //   }
    
  // }


  // // Identify overlapping ids
  // // Compute coordinate transformation between a pair of matching points
  // void Charucoboard::ComputeTransformation(const cv::String xmlfile1, const cv::String xmlfile2,
  // 					   const Triangulator& tri,
  // 					   const TriMethod method, const cv::String rtfile) const
  // {
  //   // Read the sets of matching corners and their IDs from the pair of xml files
  //   cv::Mat_<double> A_left, A_right; // Corresponding corners in pose A
  //   cv::Mat_<int> A_ids; // Corresponding corner IDs in pose A
  //   ReadCorners(xmlfile1, A_left, A_right, A_ids);

  //   // Read the sets of matching corners and their IDs from the pair of xml files
  //   cv::Mat_<double> B_left, B_right; // Corresponding corners in pose B
  //   cv::Mat_<int> B_ids; // Corresponding corner IDs in pose B
  //   ReadCorners(xmlfile2, B_left, B_right, B_ids);

  //   // Get the set of common corner IDs
  //   std::set<int> commonIDs;
  //   IdentifyCommonCorners(A_ids, B_ids, commonIDs);
  //   const int nCommonIDs = static_cast<int>(commonIDs.size());
  //   CV_Assert(nCommonIDs>6 && "Charucoboard::ComputeTransformation()- Fewer than 6 common corners found between poses.");
    
  //   // Cartesian coordinates of common pixels in the two images from triangulation
  //   cv::Mat_<double> A_cnrCoords, B_cnrCoords;
  //   ComputeCorners(A_left, A_right, A_ids, commonIDs, tri, method, A_cnrCoords);
  //   ComputeCorners(B_left, B_right, B_ids, commonIDs, tri, method, B_cnrCoords);
  //   CV_Assert((A_cnrCoords.size()==B_cnrCoords.size() && B_cnrCoords.cols==3 && B_cnrCoords.rows==nCommonIDs)
  // 	      && "Charucoboard::ComputeTransformation()- Unexpected number of common corners found.");
    
  //   // Compute the rigid body transformation taking B_cnrCoord to A_cnrCoords.
  //   // That is, R B_cnrCoords -> A_cnrCoords.

  //   // Centroid of each point set
  //   std::array<double, 3> A_centroid({0.,0.,0.}), B_centroid({0.,0.,0.});
  //   for(int i=0; i<nCommonIDs; ++i)
  //     for(int j=0; j<3; ++j)
  // 	{
  // 	  A_centroid[j] += A_cnrCoords(i,j);
  // 	  B_centroid[j] += B_cnrCoords(i,j);
  // 	}
  //   for(int k=0; k<3; ++k)
  //     {
  // 	A_centroid[k] /= static_cast<double>(nCommonIDs);
  // 	B_centroid[k] /= static_cast<double>(nCommonIDs);
  //     }
    
  //   // Compute the covariant matrix
  //   cv::Mat_<double> covMat(3,3);
  //   covMat = cv::Mat_<double>::zeros(3,3);
  //   for(int i=0; i<3; ++i)
  //     for(int j=0; j<3; ++j)
  // 	for(int p=0; p<nCommonIDs; ++p)
  // 	  covMat(i,j) += (A_cnrCoords(p,i)-A_centroid[i])*(B_cnrCoords(p,j)-B_centroid[j]);

  //   // SVD of the covariance matrix
  //   cv::Mat_<double> sVals, Umat, Vtmat;
  //   cv::SVD::compute(covMat, sVals, Umat, Vtmat);
  //   double det = cv::determinant(Umat*Vtmat);
  //   cv::Mat_<double> resetVals = cv::Mat_<double>::eye(3,3);
  //   if(det<0.) resetVals(2,2) = -1.;
  //   cv::Mat_<double> RMat(3,3);
  //   RMat = Vtmat.t()*resetVals*Umat.t();
  //   CV_Assert(std::abs(cv::determinant(RMat)-1.)<1.e-6 &&
  // 	      "Charucoboard::ComputeTransformation()- inconsistent determinant for rotation.");

  //   // Translation
  //   cv::Mat_<double> TMat(3,1);
  //   for(int i=0; i<3; ++i)
  //     {
  // 	TMat(i,0) = B_centroid[i];
  // 	for(int j=0; j<3; ++j)
  // 	  TMat(i,0) -= RMat(i,j)*A_centroid[j];
  //     }

  //   // Save reconstructed points from the two poses, R, and T
  //   cv::FileStorage fs(rtfile, cv::FileStorage::WRITE);
  //   CV_Assert(fs.isOpened() && "Charucoboard::ComputeTransformation()- Could not open file to write.");
  //   cvWriteComment(*fs, (char*)"Corresponding corner from pose 1", 0);
  //   fs << "point_set_1" << A_cnrCoords;
  //   cvWriteComment(*fs, (char*)"Corresponding corner from pose 2", 0);
  //   fs << "point_set_2" << B_cnrCoords;
  //   cvWriteComment(*fs, (char*)"Rotation and translation mapping point set 2 onto point set 1.", 0);
  //   fs << "rot_mat" << RMat;
  //   fs << "trans_mat" <<TMat;
  //   fs.release();
  //   return;
  // }

}
