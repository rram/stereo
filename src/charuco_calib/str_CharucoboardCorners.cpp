// Sriramajayam

#include <str_CharucoboardCorners.h>
#include <map>

namespace str
{
  // Anonymous namespace for helper
  namespace
  {
    // Helper method to identify common markers
    // A[Aindx[i]] = B[Bindx[i]].
    void GetMarkerCorrespondences(const std::vector<int>& A, const std::vector<int>& B,
				  std::vector<int>& Aindx, std::vector<int>& Bindx)
    {
      Aindx.clear(); Bindx.clear();
      const int nAmarkers = static_cast<int>(A.size());
      const int nBmarkers = static_cast<int>(B.size());

      if(nAmarkers==0 || nBmarkers==0) return;
    
      // Create mapping from markers->index for list B
      std::map<int, int> Bmarker2indx;
      for(int i=0; i<nBmarkers; ++i)
	Bmarker2indx[B[i]] = i;

      // Detect coincident markers
      for(int i=0; i<nAmarkers; ++i)
	{
	  int  mnum = A[i];
	  // Does 'mnum' appear in list B?
	  auto it = Bmarker2indx.find(mnum);
	  if(it!=Bmarker2indx.end())
	    {
	      Aindx.push_back( i );
	      Bindx.push_back( it->second );
	    }
	}

      // Sanity check
      const int nCommon = static_cast<int>(Aindx.size());
      CV_Assert(static_cast<int>(Bindx.size())==nCommon && "GetMarkerCorrespondences- Unexpected scenario");
      
      for(int i=0; i<nCommon; ++i)	
	CV_Assert(A[Aindx[i]]==B[Bindx[i]] && "GetMarkerCorrespondences- Unexpected scenario");

      // Done
      return;
    }
  }


  // Match: Helper function that given two boards, returns 2 new boards having
  // overlapping markers in respective images
  void CharucoboardCorners::Match(const CharucoboardCorners& A, const CharucoboardCorners& B,
				  CharucoboardCorners& newA, CharucoboardCorners& newB)
  {
    A.Check();
    B.Check();
    const int& nImages = A.nImages;
    CV_Assert(B.nImages==nImages);
    newA.Clear();
    newB.Clear();
    
    newA.nImages = nImages;
    newA.cornerIDs.resize(nImages);
    newA.corners2D.resize(nImages);
    newA.corners3D.resize(nImages);
    newA.rvecs = A.rvecs;
    newA.tvecs = A.tvecs; 

    newB.nImages = nImages;
    newB.cornerIDs.resize(nImages);
    newB.corners2D.resize(nImages);
    newB.corners3D.resize(nImages);
    newB.rvecs = B.rvecs;
    newB.tvecs = B.tvecs; 

    // Pack common markers, image-by-image
    for(int f=0; f<nImages; ++f)
      {
	// Identify overlapping markers
	std::vector<int> aindx({}), bindx({});
	GetMarkerCorrespondences(A.cornerIDs[f], B.cornerIDs[f], aindx, bindx);
	const int nCommon = static_cast<int>(aindx.size());

	newA.cornerIDs[f].resize(nCommon);
	newA.corners2D[f].resize(nCommon);
	newA.corners3D[f].resize(nCommon);		

	newB.cornerIDs[f].resize(nCommon);
	newB.corners2D[f].resize(nCommon);
	newB.corners3D[f].resize(nCommon);	
	
	for(int i=0; i<nCommon; ++i)
	  {
	    newA.cornerIDs[f][i] = A.cornerIDs[f][aindx[i]];
	    newB.cornerIDs[f][i] = B.cornerIDs[f][bindx[i]];

	    newA.corners2D[f][i] = A.corners2D[f][aindx[i]];
	    newB.corners2D[f][i] = B.corners2D[f][bindx[i]];

	    newA.corners3D[f][i] = A.corners3D[f][aindx[i]];
	    newB.corners3D[f][i] = B.corners3D[f][bindx[i]];
	  }
      }

    // Sanity checks
    newA.Check();
    newB.Check();

    // done
    return;
  }
    

  // Sanity checks
  void CharucoboardCorners::Check() const
  {
    CV_Assert(nImages>0);
    CV_Assert(nImages==static_cast<int>(cornerIDs.size()));
    CV_Assert(nImages==static_cast<int>(corners2D.size()));
    CV_Assert(nImages==static_cast<int>(corners3D.size()));
    CV_Assert(nImages==static_cast<int>(rvecs.size()));
    CV_Assert(nImages==static_cast<int>(tvecs.size()));
    for(int f=0; f<nImages; ++f)
      {
	const unsigned int nCorners = cornerIDs[f].size();
	CV_Assert(nCorners==corners2D[f].size());
	CV_Assert(nCorners==corners3D[f].size());
      }
    return;
  }

  // Clear all contents
  void CharucoboardCorners::Clear()
  {
    nImages = 0;
    cornerIDs.clear();
    corners2D.clear();
    corners3D.clear();
    rvecs.clear();
    tvecs.clear();
    return;
  }
  
}
