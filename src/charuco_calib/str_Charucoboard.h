// Sriramajayam

#ifndef STR_CHARUCO_BOARD_H
#define STR_CHARUCO_BOARD_H

#include <opencv2/core.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/aruco/charuco.hpp>
#include <map>

namespace str
{
  struct CharucoBoardImage
  {
  public:

    // Default constructor
    inline CharucoBoardImage()
    { imgName.clear(); }

    inline CharucoBoardImage(const cv::String name)
      :imgName(name) {}
    
    cv::String imgName;
    std::vector<int> markerIDs;
    std::vector<std::vector<cv::Point2f>> markerCorners;
    cv::Mat charucoCorners;
    cv::Mat charucoIDs;
    int nMarkers;
    int nCorners;
  };

  class Charucoboard
  {
  public:
    //! Default constructor
    inline Charucoboard() {}
  
    //! Create with given set of parameters
    //! Create board from file
    inline Charucoboard(const cv::String filename)
    { Create(filename); }

    //! Copy constructor
    Charucoboard(const Charucoboard& Obj)
      :_board(Obj._board) {}
    
    //! Destructor does nothing
    inline virtual ~Charucoboard() {}
    
    //! Create board from file
    void Create(const cv::String xmlfile);

    //! Returns the board
    const cv::Ptr<cv::aruco::CharucoBoard>& GetBoard() const
      { return _board; }
      
    //! Draws a board with the specified margin and marker borders
    void Draw(const cv::String filename,
	      const int marginSize=0, const int borderBits=1);

    //! Main functionality: detect corners in an image
    inline void Detect(const cv::String imgname, CharucoBoardImage& cbImage) const
    {
      cbImage.imgName = imgname;
      Detect(cbImage);
    }
    
    //! Main functionality: detect corners in an image
    void Detect(CharucoBoardImage& cbImage) const;

    //! Compute corresponding corners in a pair of images
    void MatchImages(const cv::String img1, const cv::String img2, cv::String ptsfile) const;

    //! Identify and print corresponding corners in a pair of poses.
    //! Print them as separate xmlfiles
    void MatchPoses(const cv::String pose1xml, const cv::String pose2xml,
		    const cv::String pose1subxml, const cv::String pose2subxml) const;

    //! Compute coordinate transformation between a pair of matching points
    //! xmlfile1: Matching pairs of pixels and their ids- as produced by the function Match() for pose 1
    //! xmlfile2: Matching pairs of pixels and their ids- as produced by the function Match() for pose 2
    //! tri: Triangulator to use
    //! method: Triangulation method to use
    //! rtfile: File in which to print rotations and translations computed
    //void ComputeTransformation(const cv::String xmlfile1, const cv::String xmlfile2,
    //			       const Triangulator& tri, const TriMethod method,
    //			       const cv::String rtfile) const;
    
    //! Saves the board details to an xml file
    void Save(const cv::String xmlfile) const;

    cv::Ptr<cv::aruco::CharucoBoard> _board;

  private:
    // Static map from strings to dictionary in the library
    static const std::map<cv::String, cv::aruco::PredefinedDictionaryType> _dictionaryMap;
    cv::Ptr<cv::aruco::Dictionary> dictionary;

    /*// Helper function to read pixels and corner ids
    static void ReadCorners(const cv::String xmlfile,
			    cv::Mat_<double>& left, cv::Mat_<double>& right,
			    cv::Mat_<int>& ids);*/
  };

}
#endif
