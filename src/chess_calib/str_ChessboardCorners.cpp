// Sriramajayam

#include <str_ChessboardCorners.h>
#include <iostream>

namespace str
{
  // Read data from file
  void ChessboardCorners::Create(const cv::String xmlfile)
  {
    std::cout<<"\nCreating chessboardcorners from file "<<xmlfile<<std::flush;
    
    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() &&
	      "str::ChessboardCorners::ChessboardCorners()- Could not open file for reading.");

    // Read the number of images
    auto fn = fs["image_count"];
    CV_Assert(!fn.empty() &&
	      "ChessboardCorners::Create()- Could not read field image_count.");
    _nImages = static_cast<unsigned int>(static_cast<int>(fs["image_count"]));
    const int nImages = static_cast<int>(_nImages);
    
    // Read the corners coordinates in 2D/3D
    cv::Mat c2D, c3D;
    fn = fs["corners_2D"];
    CV_Assert(!fn.empty() && "str::ChessboardCorners::Create()- Could not read field corners_2D");
    cv::read(fn, c2D);
    
    fn = fs["corners_3D"];
    CV_Assert(!fn.empty() && "str::ChessboardCorners::Create()- Could not read field corners_3D");
    cv::read(fn, c3D);
    CV_Assert( (c2D.rows==nImages && c3D.rows==nImages) &&
	       "str::ChessboardCorners::Create()- unexpected number of images");
    const int nCorners = c2D.cols;
    CV_Assert( c3D.cols==nCorners &&
	       "str::ChessboardCorners::Create()- unexpected number of corners");
    
    _cornersIn3D.resize(nImages);
    _cornersInImages.resize(nImages);
    for(int i=0; i<nImages; ++i)
      {
	auto& vec2 = _cornersInImages[i];
	auto& vec3 = _cornersIn3D[i];
	vec2.resize(nCorners);
	vec3.resize(nCorners);
	for(int c=0; c<nCorners; ++c)
	  {
	    vec2[c] = c2D.at<cv::Point2f>(i,c);
	    vec3[c] = c3D.at<cv::Point3f>(i,c);
	  }
      }
    
    // Read the rotation and translation vectors
    cv::Mat R, T;
    fn = fs["rotations"];
    CV_Assert(!fn.empty() && "str::ChessboardCorners::Create()- Could not read field rotations");
    cv::read(fn, R);

    fn = fs["translations"];
    CV_Assert(!fn.empty() && "str::ChessboardCorners::Create()- Could not read field translations");
    cv::read(fn, T);
    
    CV_Assert( (R.rows==nImages && T.rows==nImages) &&
	       "str::ChessboardCorners::Create()- unexpected number of images");
    CV_Assert( (R.cols==3 && T.cols==3) &&
	       "str::ChessboardCorners::Create()- unexpected number of vector components.");
    
    _rvecs.resize(nImages);
    _tvecs.resize(nImages);
    for(int i=0; i<nImages; ++i)
      {
	_rvecs[i] = R.row(i).t();
	_tvecs[i] = T.row(i).t();
      }
    
    fs.release();
  }

  
  // Save data about corners to file
  void ChessboardCorners::Save(const cv::String xmlfile) const
  {
    std::cout<<"\nSaving chessboardcorners to file "<<xmlfile<<std::flush;
    
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "str::ChessboardCorners::Save()- Could not open file for writing.");

    // Write the number of images
    const int nImages = static_cast<int>(_nImages);
    fs << "image_count" << nImages;

    // Write the corners in 2D/3D
    const int nCorners = static_cast<int>(_cornersInImages[0].size());
    cv::Mat corners3D(nImages, nCorners, CV_32FC3);
    cv::Mat corners2D(nImages, nCorners, CV_32FC2);
    for(int i=0; i<nImages; ++i)
      {
	const auto& vec3 = _cornersIn3D[i];
	const auto& vec2 = _cornersInImages[i];
	for(int c=0; c<nCorners; ++c)
	  {
	    corners3D.at<cv::Point3f>(i,c) = vec3[c];
	    corners2D.at<cv::Point2f>(i,c) = vec2[c];
	  }
      }
    fs<<"corners_3D"<<corners3D;
    fs<<"corners_2D"<<corners2D;
	
    // rotation and translation vecs from 3D -> image
    cv::Mat R(nImages, 3, CV_64F);
    cv::Mat T(nImages, 3, CV_64F);
    for(int i=0; i<nImages; ++i)
      {
	const auto& r = _rvecs[i];
	const auto& t = _tvecs[i];
	for(int j=0; j<3; ++j)
	  {
	    R.at<double>(i,j) = r.at<double>(j,0);
	    T.at<double>(i,j) = t.at<double>(j,0);
	  }
      }
    fs<<"rotations"<<R;
    fs<<"translations"<<T;
    
    fs.release();
  }

 
}
