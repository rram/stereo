// Sriramajayam

#include <str_Camera.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

#include <str_Charucoboard.h>
#include <str_CharucoboardCorners.h>

namespace str
{
  
  // Main functionality:
  // calibrate this camera from a given set of charuco board images
  void Camera::Calibrate(const Charucoboard& charu, const cv::String xmlfile,
			 CharucoboardCorners& cbCorners)
  {
    // Reset calibration flag
    _isCalibrated = false;
    
    // Read the number of images and filenames from the xml file
    std::cout<<"\nReading XML file "<<xmlfile<<" for camera calibration"<<std::flush;

    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() && "Camera::Calibrate- Could not open XML file ");

    // Read
    int image_count = 0;
    auto fn = fs["image_count"];
    CV_Assert(!fn.empty() && "Camera::Calibrate- Could not read image_count");
    cv::read(fn, image_count, 0);

    cv::Size imgSize;
    fn = fs["image_width"];
    CV_Assert(!fn.empty() && "Camera::Calibrate- Could not read image_width");
    cv::read(fn, imgSize.width, 0);

    fn = fs["image_height"];
    CV_Assert(!fn.empty() && "Camera::Calibrate- Could not read image_height");
    cv::read(fn, imgSize.height, 0);

    // Copy the image size
    _imgSize = imgSize;

    std::vector<cv::String> calib_images({});
    fn = fs["calib_images"];
    CV_Assert(!fn.empty() && "Camera::Calibrate- Could not read calib_images");
    cv::read(fn, calib_images);
    CV_Assert(static_cast<int>(calib_images.size())==image_count &&
	      "Camera::Calibrate- Inconsistent number of images");

    // Close file
    fs.release();

    // Chessboard size
    auto boardSize = charu._board->getChessboardSize();
    const int xsize = boardSize.width;
    const double sqSize = charu._board->getSquareLength();
    
    // Aliases
    cbCorners.Clear();
    cbCorners.nImages = static_cast<unsigned int>(image_count);
    auto& cornerIDs = cbCorners.cornerIDs;
    auto& corners2D = cbCorners.corners2D;
    auto& corners3D = cbCorners.corners3D;
    auto& rvecs = cbCorners.rvecs;
    auto& tvecs = cbCorners.tvecs;

    // Set sizes
    cornerIDs.resize(image_count);
    corners2D.resize(image_count);
    corners3D.resize(image_count);
    
    // Detect marker corners and IDs in each image
    for(int f=0; f<image_count; ++f)
      {
	cornerIDs[f].clear();
	corners2D[f].clear();
	corners3D[f].clear();
	
	// Detect corners in this image
	CharucoBoardImage cbImage;
	cbImage.imgName = calib_images[f];
	charu.Detect(cbImage);

	// Concatenate all charuco corners
	for(int i=0; i<cbImage.nCorners; ++i)
	  {
	    // ID
	    cornerIDs[f].push_back( cbImage.charucoIDs.at<int>(i,0) );

	    // image corners
	    corners2D[f].push_back( cbImage.charucoCorners.at<cv::Point2f>(i,0) );

	    // 3D coordinates
	    int id =  cbImage.charucoIDs.at<int>(i,0);
	    int col = id/(xsize-1);
	    int row = id%(xsize-1);
	    corners3D[f].push_back( cv::Point3f(static_cast<float>(col*sqSize), static_cast<float>(row*sqSize), 0.) );
	  }
      }
    
    // Calibrate the camera: First pass
    _camMatrix = cv::Mat::eye(3,3,CV_64F);
    _distCoeffs = cv::Mat::zeros(5,1,CV_64F);
    double rmsError =
      calibrateCameraCharuco(corners2D, cornerIDs, charu._board,
			     imgSize, _camMatrix, _distCoeffs,
			     rvecs, tvecs,
			     cv::CALIB_FIX_ASPECT_RATIO+cv::CALIB_ZERO_TANGENT_DIST+cv::CALIB_FIX_K3);
    
    // Sanity checks
    CV_Assert(static_cast<int>(rvecs.size())==image_count && "Camera::Calibrate- Unexpected size of rotations");
    CV_Assert(static_cast<int>(tvecs.size())==image_count && "Camera::Calibrate- Unexpected size of translations");
    CV_Assert(rvecs[0].type()==CV_64F && "Camera::Calibrate: Unexpected rotation type");
    CV_Assert(tvecs[0].type()==CV_64F && "Camera::Calibrate: Unexpected translation type");
    std::cout<<"\nCamera calibration error at stage 1: "<<rmsError<<std::flush;
    
    // Calibrate camera: Second pass
    rmsError =  cv::aruco::calibrateCameraCharuco(corners2D, cornerIDs, charu._board, imgSize,
						  _camMatrix, _distCoeffs,
						  rvecs, tvecs,
						  cv::CALIB_USE_INTRINSIC_GUESS+cv::CALIB_ZERO_TANGENT_DIST+cv::CALIB_FIX_K3);
    std::cout<<"\nCamera calibration error at stage 2: "<<rmsError<<std::flush;
    
    // Calibrate camera: Third pass
    rmsError =  cv::aruco::calibrateCameraCharuco(corners2D, cornerIDs,
						  charu._board, imgSize,
						  _camMatrix, _distCoeffs,
						  rvecs, tvecs,
						  cv::CALIB_USE_INTRINSIC_GUESS+cv::CALIB_FIX_K3);
    std::cout<<"\nCamera calibration error at stage 3: "<<rmsError<<std::flush;

    // Calibration is done
    _isCalibrated = true;

    // Compute distortion maps
    ComputeDistortionMaps();

    // done
    return;
  }


  // Computes the error in calibration
  double Camera::
  ComputeCalibrationError(const CharucoboardCorners& corners) const
  {
    CV_Assert(_isCalibrated && "Camera::ComputeCalibrationError()- Not calibrated.");
    
    // Reprojected points in each image
    std::vector<cv::Point2f> reproj;
    double totErr = 0.;
    int nPoints = 0;
    const int nImages = corners.nImages;
    for(int i=0; i<nImages; ++i)
      {
	// Project points from 3D coordinates onto image plane
	const int nCorners = corners.corners2D[i].size();
	cv::projectPoints(cv::Mat(corners.corners3D[i]), corners.rvecs[i], corners.tvecs[i], _camMatrix, _distCoeffs, reproj);
	CV_Assert(static_cast<int>(reproj.size())==nCorners && "Camera::ComputeCalibrationError()- Unexpected number of projected points.");
	nPoints += nCorners;
	const auto& imPts = corners.corners2D[i];
	for(int c=0; c<nCorners; ++c)
	  totErr +=
	    (imPts[c].x-reproj[c].x)*(imPts[c].x-reproj[c].x) +
	    (imPts[c].y-reproj[c].y)*(imPts[c].y-reproj[c].y);
      }
    
    // Return rms error
    return std::sqrt(totErr/static_cast<double>(nPoints));
  }
}

