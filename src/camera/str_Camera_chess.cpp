// Sriramajayam

#include <str_Camera.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

#include <str_Chessboard.h>
#include <str_ChessboardCorners.h>

namespace str
{
  // Main functionality: calibrate this camera
  void Camera::Calibrate(const Chessboard& cb, ChessboardCorners& corners)
  {
    // Reset calibration flag
    _isCalibrated = false;

    // Copy the image size
    _imgSize = cb._images._imageSize;
    
    // Aliases
    auto& im = cb._images;
    auto& cornersIn3D = corners._cornersIn3D;
    auto& cornersInImages = corners._cornersInImages;
    auto& rvecs = corners._rvecs;
    auto& tvecs = corners._tvecs;
    const int nImages = static_cast<int>(im._nImages);
    corners._nImages = im._nImages;
    
    // Read corners in images
    cornersInImages.resize(nImages);
    const int nExpCorners = cb._boardSize.height*cb._boardSize.width;
    bool flag;
    cv::Mat img;
    const auto termcrit = cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS,30,0.1);
    
    for(int f=0; f<nImages; ++f)
      {
	// Read this image
	std::cout<<"\nReading image "<<im._fileNames[f]<<std::flush;
	img = cv::imread(im._fileNames[f], cv::IMREAD_GRAYSCALE);
	CV_Assert(img.empty()==false && "str::Camera::Calibrate()- Could not read image.");
	CV_Assert(_imgSize==img.size() && "str::Camera::Calibrate()- Unexpected image size.");
	
	// Find corners in this image
	std::cout<<"\nFinding chessboard corners "<<std::flush;
	flag = cv::findChessboardCorners(img, cb._boardSize,
					 cornersInImages[f],
					 cv::CALIB_CB_FAST_CHECK+cv::CALIB_CB_ADAPTIVE_THRESH);
	CV_Assert(flag==true && "str::Camera::Calibrate()- Could not find corners in image.");
	CV_Assert( (static_cast<int>(cornersInImages[f].size())==nExpCorners) &&
		   "str::Camera::Calibrate()- Could not find all corners in image.");
	
	// Refine to subpixel accuracy
	// assumes at least 20px separation between corners in image
	std::cout<<"\nFinding corner subpixels "<<std::flush;
	cv::cornerSubPix(img, cornersInImages[f], cv::Size(10,10), cv::Size(-1,-1), termcrit);
	
	// Plot the identified corners
	std::cout<<"\nPlotting identified corners \n"<<std::flush;
	cv::Mat img_color;
	cv::cvtColor(img, img_color, cv::COLOR_GRAY2BGR);
	cv::drawChessboardCorners(img_color, cb._boardSize,
				  cornersInImages[f], flag);
	CV_Assert(flag==true && "str::Camera::Calibrate()- pattern not found.");
	std::string filename = "calibcorners"+std::to_string(f)+".jpg";
	cv::imwrite(filename, img_color);
      }
    
    // Coordinates of corners in chessboard
    cornersIn3D.resize(1);
    for(int i=0; i<cb._boardSize.height; ++i)
      for(int j=0; j<cb._boardSize.width; ++j)
	cornersIn3D[0].push_back
	  (cv::Point3f(static_cast<float>(j*cb._squareSize),
		       static_cast<float>(i*cb._squareSize), 0.));
    cornersIn3D.resize(im._nImages, cornersIn3D[0]);
    
    // Calibrate the camera
    _camMatrix = cv::Mat::eye(3,3,CV_64F);
    _distCoeffs = cv::Mat::zeros(5,1,CV_64F);
    
    // solve for parameters in an incremental way
    double rmsErr =
      cv::calibrateCamera(cornersIn3D, cornersInImages,
			  _imgSize, _camMatrix, _distCoeffs,
			  rvecs, tvecs,
			  cv::CALIB_FIX_ASPECT_RATIO+cv::CALIB_ZERO_TANGENT_DIST+cv::CALIB_FIX_K3);
    
    // Sanity checks
    CV_Assert(static_cast<int>(rvecs.size())==nImages && "str::Camera::Calibrate()- Unexpected number of rotations.");
    CV_Assert(static_cast<int>(tvecs.size())==nImages && "str::Camera::Calibrate()- Unexpected number of translations.");
    CV_Assert(rvecs[0].type()==CV_64F && "str::Camera::Calibrate()- Unexpected rotation type.");
    CV_Assert(tvecs[0].type()==CV_64F && "str::Camera::Calibrate()- Unexpected rotation type.");
    std::cout<<"\nCamera calibration, error at stage 1: "<<rmsErr<<std::flush;
    
    rmsErr = cv::calibrateCamera(cornersIn3D, cornersInImages,
				 _imgSize, _camMatrix, _distCoeffs,
				 rvecs, tvecs,
				 cv::CALIB_USE_INTRINSIC_GUESS+cv::CALIB_ZERO_TANGENT_DIST+cv::CALIB_FIX_K3);
    std::cout<<"\nCamera calibration, error at stage 2: "<<rmsErr<<std::flush;
    
    rmsErr = cv::calibrateCamera(cornersIn3D, cornersInImages,
				 _imgSize, _camMatrix, _distCoeffs,
				 rvecs, tvecs,
				 cv::CALIB_USE_INTRINSIC_GUESS+cv::CALIB_FIX_K3);
    std::cout<<"\nCamera calibration, error at stage 3: "<<rmsErr<<std::flush;
    
    // Calibration is done
    _isCalibrated = true;
    
    // Compute distortion maps
    ComputeDistortionMaps();

    // done
    return;
  }


  // Computes the error in calibration
  double Camera::
  ComputeCalibrationError(const ChessboardCorners& corners) const
  {
    CV_Assert(_isCalibrated && "Camera::ComputeCalibrationError()- Not calibrated.");
    
    // Reprojected points in each image
    std::vector<cv::Point2f> reproj;
    double totErr = 0.;
    int nPoints = 0;
    const auto& nImages = corners._nImages;
    const unsigned int nCorners = corners._cornersInImages[0].size();
    for(unsigned int i=0; i<nImages; ++i)
      {
	// Project points from 3D coordinates onto image plane
	cv::projectPoints(cv::Mat(corners._cornersIn3D[i]), corners._rvecs[i], corners._tvecs[i], _camMatrix, _distCoeffs, reproj);
	CV_Assert(reproj.size()==nCorners && "str::Camera::ComputeCalibrationError()- Unexpected number of projected points.");
	nPoints += nCorners;
	const auto& imPts = corners._cornersInImages[i];
	for(unsigned int c=0; c<nCorners; ++c)
	  totErr +=
	    (imPts[c].x-reproj[c].x)*(imPts[c].x-reproj[c].x) +
	    (imPts[c].y-reproj[c].y)*(imPts[c].y-reproj[c].y);
      }

    // Return rms error
    return std::sqrt(totErr/static_cast<double>(nPoints));
  }
  
}
