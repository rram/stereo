// Sriramajayam

#include <str_Camera.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

namespace str
{
  // Default constructor.
  Camera::Camera()
    :_imgSize(0,0),
     _isCalibrated(false) {}

  // Construct a calibrated camera from a file
  Camera::Camera(const cv::String xmlfile)
    :Camera()
  { Create(xmlfile); }
  
  // Copy constructor
  Camera::Camera(const Camera& obj)
    :_camMatrix(obj._camMatrix),
     _distCoeffs(obj._distCoeffs),
     _imgSize(obj._imgSize),
     _mapx(obj._mapx),
     _mapy(obj._mapy),
     _isCalibrated(obj._isCalibrated) {}
  
  
  // Destructor
  Camera::~Camera() {}

  // Returns the image size
  const cv::Size& Camera::GetImageSize() const
  { return _imgSize; }
  
  // Returns the camera matrix
  const cv::Mat& Camera::GetCameraMatrix() const
  {
    CV_Assert(_isCalibrated && "str::Camera::GetCameraMatrix()- Not calibrated.");
    return _camMatrix;
  }
  
  // Returns the distortion coefficients
  const cv::Mat& Camera::GetDistortionCoeffs() const
  {
    CV_Assert(_isCalibrated && "str::Camera::GetCameraMatrix()- Not calibrated.");
    return _distCoeffs;
  }
  
  
  // Returns the camera matrix and distortion coefficients
  void Camera::GetIntrinsics(cv::Mat& camMatrix,
			     cv::Mat& distCoeffs) const
  {
    CV_Assert(_isCalibrated && "str::Camera::GetCameraIntrinsics()- Not calibrated.");
    camMatrix = _camMatrix;
    distCoeffs = _distCoeffs;
    return;
  }
  
  // Set the camera matrix and distortion coefficients
  void Camera::SetIntrinsics(const cv::Mat camMatrix,
			     const cv::Mat distCoeffs,
			     const cv::Size imgSize)
  {
    _camMatrix = camMatrix;
    _distCoeffs = distCoeffs;
    _imgSize = imgSize;
    _isCalibrated = true;
    ComputeDistortionMaps();
  }
     
  
  // Undistorts given image using calibrated parameters
  void Camera::Undistort(const cv::Mat& img_in,  cv::Mat& img_out) const
  {
    CV_Assert(_isCalibrated && "str::Camera::Undistort()- Camera not calibrated.");
    CV_Assert(!img_in.empty() && "str::Camera::Undistort()- Could not open file.");
    cv::remap(img_in, img_out, _mapx, _mapy, cv::INTER_LINEAR);
    return;
  }

  // Undistorts given image using calibrated parameters
  void Camera::Undistort(const cv::String imgfile, cv::Mat& img_out) const
  {
    cv::Mat img_in = cv::imread(imgfile, cv::IMREAD_GRAYSCALE);
    CV_Assert(img_in.empty()==false && "str::Camera::Undistort()- Could not open image.");
    Undistort(img_in, img_out);
  }

  

  // Print camera details to file
  void Camera::Save(const cv::String filename) const
  {
    std::cout<<"\nSaving camera to file "<<filename<<std::flush;
    
    CV_Assert(_isCalibrated && "str::Camera::Save()- camera not calibrated.");
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "str::Camera::Save()- could not open file.");

    // image size
    fs << "image_height" << _imgSize.height;
    fs << "image_width" << _imgSize.width;
    
    // camera matrix
    fs<<"cam_matrix"<<_camMatrix;
    
    // distortion coefficients
    fs<<"dist_coeffs"<<_distCoeffs;
    
    fs.release();
  }
  
  // Read camera details from file
  void Camera::Create(const cv::String filename)
  {
    std::cout<<"\nCreating camera from file "<<filename<<std::flush;
    
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() && "str::Camera::Create()- could not open file.");
    
    // Image size
    auto fn = fs["image_width"];
    CV_Assert(!fn.empty() && "str::Camera::Create()- could not read field image_width");
    cv::read(fn, _imgSize.width, 0);
    
    fn = fs["image_height"];
    CV_Assert(!fn.empty() && "str::Camera::Create()- could not read field image_height");
    cv::read(fn, _imgSize.height, 0);
    
    // Camera matrix
    fn = fs["cam_matrix"];
    CV_Assert(!fn.empty() && "str::Camera::Create()- could not read field cam_Matrix");
    cv::read(fn, _camMatrix);

    // Distortion coeffs
    fn = fs["dist_coeffs"];
    CV_Assert(!fn.empty() && "str::Camera::Create()- could not read field dist_Coeffs");
    cv::read(fn, _distCoeffs);

    fs.release();

    // Camera is assumed to be calibrated
    _isCalibrated = true;
    
    // Check camera matrix
    CV_Assert(_camMatrix.size()==cv::Size(3,3) &&
	      "str::Camera::Create()- Unexpected camera matrix dimensions.");
    double isSmall = std::abs(_camMatrix.at<double>(0,1))+
      std::abs(_camMatrix.at<double>(1,0))+
      std::abs(_camMatrix.at<double>(2,0))+
      std::abs(_camMatrix.at<double>(2,1));
    CV_Assert((isSmall< 1.e-6) &&
	      "str::Camera::Create()- Unexpected terms in camera matrix.");
    
    // Compute distortion maps
    ComputeDistortionMaps();
  }

  
  // Update the distortion mapping
  void Camera::ComputeDistortionMaps()
  {
    // no rectification done here
    cv::initUndistortRectifyMap(_camMatrix, _distCoeffs,
				cv::Mat::eye(3,3,CV_64F),
				_camMatrix, _imgSize,
				CV_32FC1, _mapx, _mapy);
  }
  
}
